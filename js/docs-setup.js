NG_DOCS={
  "sections": {
    "api": "API Reference",
    "dev": "Developer Reference",
    "gitflows": "Git Workflows"
  },
  "pages": [
    {
      "section": "api",
      "id": "index",
      "shortName": "Transcend Modules API",
      "type": "overview",
      "moduleName": "Transcend Modules API",
      "shortDescription": "",
      "keywords": "$log __anychart __app __core __data __diagnostics __drawing __esri __export __notify __reporting __resource __security __themes ability access actions add addition additional alert alert-info allow allows alt amount angular angularjs anychart api app application applications arcgis aspx assist authenticate authentication automatically base based basic behaviour benefit bind binding build builds built calculations capabilities center changes charts chunk class code common commonly component components configurable connections consist container content control controls core create csv currently data data-source database debugging defines delete dependency designed details diagnostics directive directives docs documentation download drawing dynamic dynamicform easier easily elapsed email enable engine esri exists export exposed extends factory filtering form formats formatting fucntionality functionality future general generic goal groups help helpers hiding hierarchical html http https include injectable integration interact ivborw0kggoaaaansuheugaaagqaaabtcayaaabx7cscaaaabmjlr0qa javascript large layers level library lines list loading location main majority manage manager managing map markup materials meta-data method modal module module__ modules msdn mvc nature net notification notify number object offers operations org organized overview owin people points practice prefix prefixes primary private provide pull-right re-usable read rectangles reference remain reporting reports represent resource resources reusable roles seamlessly security security-manager select service services set share simple situations small source sources split src standard strictly structure styling suggests support svg switch sync technology template tested text themes things time token top tracking transcend two-way type types update user users utilitarian utility values view work working zoom"
    },
    {
      "section": "api",
      "id": "transcend.anychart",
      "shortName": "transcend.anychart",
      "type": "overview",
      "moduleName": "transcend.anychart",
      "shortDescription": "AnyChart Module",
      "keywords": "ability add allow angular angularjs anychart api application approach automatically based benefit better bind binding bower build changes charting charts code commands components create cutting data dependency deprecate directives download easiest easily files find git goals https included install installation io isn js libraries library main module mymodule options org overview package permissions project pulled re-evaluate repo repository reusable set simply source standard storing technology transcend tsstools update view zip"
    },
    {
      "section": "api",
      "id": "transcend.anychart.directive:anyChart",
      "shortName": "anyChart",
      "type": "directive",
      "moduleName": "transcend.anychart",
      "shortDescription": "The &#39;AnyChart&#39; directive provides the ability to create &quot;AnyChart&quot; graphs and charts and bind to dynamic data.",
      "keywords": "$http $scope $timeout ability angular anychart api app bind browser browserdata btn btn-primary build chart charts chrome class code colors column columnvalue config configuration configurations controller core create ctrl data default determine directive dynamic ea example file firefox flag function gauge gaugevalue graphs height holds html ios js labels license linux locations mac max min module ng-click ng-controller ng-model note number object operating osdata override path percentage pie pievalue populate script series step system title transcend tsconfig type windows xml"
    },
    {
      "section": "api",
      "id": "transcend.anychart.service:$anyChart",
      "shortName": "$anyChart",
      "type": "service",
      "moduleName": "transcend.anychart",
      "shortDescription": "The &#39;$anyChart&#39; service is a factory to create AnyChart instances. The factory is just a wrapper to the global",
      "keywords": "$anychart $window actual addeventlistener allow angular anychart api app application call chart cleaner code controller create created directive factory function global height injected instance instances library manner object postrender pulls render service simply testctrl transcend typically width window wrapper"
    },
    {
      "section": "api",
      "id": "transcend.app",
      "shortName": "transcend.app",
      "type": "overview",
      "moduleName": "transcend.app",
      "shortDescription": "App Module",
      "keywords": "add additional angular api app application apps bootstrap bower code commands common components dependency download easiest existing files git goals help https included install installation io js libraries minified module mymodule options org overview package permissions project pull pulled repository simply size skeleton source transcend tsstools view zip"
    },
    {
      "section": "api",
      "id": "transcend.app.appConfig",
      "shortName": "transcend.app.appConfig",
      "type": "object",
      "moduleName": "transcend.app",
      "shortDescription": "Provides a single configuration object that can be used to change the behaviour, options, urls, etc for any",
      "keywords": "__builddate__ __description__ __version__ angular angularjs api app appconfig behaviour builddate change code codedocumentation components configuration constant description docs http https lg modal module myapp object options org overridden override profile serviceapi serviceurl single size transcend tss urls usersguide version"
    },
    {
      "section": "api",
      "id": "transcend.app.controller:AboutCtrl",
      "shortName": "AboutCtrl",
      "type": "controller",
      "moduleName": "transcend.app",
      "shortDescription": "The &#39;AboutCtrl&#39; controller provides control for about pages. The purpose of the about page is to simply display",
      "keywords": "aboutctrl agencies allows angular api app appconfig application applications basic build builddate business commercial company config control controller cost cots current custom database datasets december deliver depth description develop developed display effective expectations experience firms geospatial gis government html implementations js jumpstart leading leveraging local meet module monday ng-include performance pm private processes products projects provide purpose script services simply solutions specializing specific stable team technology things transcend transportation version web-common worked"
    },
    {
      "section": "api",
      "id": "transcend.app.controller:AccessControlMgrDialogCtrl",
      "shortName": "AccessControlMgrDialogCtrl",
      "type": "controller",
      "moduleName": "transcend.app",
      "shortDescription": "The &#39;AccessControlMgrDialogCtrl&#39; controller holds the logic responsible for handling roles and permissions within the",
      "keywords": "$modalinstance $rolemanager $scope access accesscontrolmgrdialogctrl api app control controller handling holds logic menu permissions responsible roles security securitymanagerconfig transcend"
    },
    {
      "section": "api",
      "id": "transcend.app.controller:AccountDialogCtrl",
      "shortName": "AccountDialogCtrl",
      "type": "controller",
      "moduleName": "transcend.app",
      "shortDescription": "The &#39;AccountDialogCtrl&#39; controller handles  account dialog menu options, and creates the &#39;My Info&#39; tab, the",
      "keywords": "$authenticate $modalinstance $scope account accountdialogctrl api app change controller creates dialog handles info menu options password privileges security tab transcend"
    },
    {
      "section": "api",
      "id": "transcend.app.controller:AccountRegistrationDialogCtrl",
      "shortName": "AccountRegistrationDialogCtrl",
      "type": "controller",
      "moduleName": "transcend.app",
      "shortDescription": "The &#39;AccountRegistrationDialogCtrl&#39; controller controls the Account Registration Dialog menu.",
      "keywords": "$loading $modalinstance $scope account accountregistrationdialogctrl api app controller controls dialog menu registration transcend"
    },
    {
      "section": "api",
      "id": "transcend.app.controller:ChangePasswordCtrl",
      "shortName": "ChangePasswordCtrl",
      "type": "controller",
      "moduleName": "transcend.app",
      "shortDescription": "The &#39;ChangePasswordCtrl&#39; controller controls the Account Registration Dialog menu.",
      "keywords": "$notify $scope account api app changepasswordctrl controller controls core dialog menu registration security service transcend"
    },
    {
      "section": "api",
      "id": "transcend.app.controller:ErrorReportCtrl",
      "shortName": "ErrorReportCtrl",
      "type": "controller",
      "moduleName": "transcend.app",
      "shortDescription": "The &#39;ErrorReportCtrl&#39; controller holds the logic for the error reporting menu, which sends an email with the details",
      "keywords": "$authenticate $loading $log $modalinstance $notify $scope $string api app appconfig controller core details email error errorreportctrl experienced holds logic menu reporting security sends transcend user written"
    },
    {
      "section": "api",
      "id": "transcend.app.controller:LogCtrl",
      "shortName": "LogCtrl",
      "type": "controller",
      "moduleName": "transcend.app",
      "shortDescription": "The &#39;LogCtrl&#39; controller holds the logic for using the logging functions.",
      "keywords": "$log $scope api app controller functions holds logctrl logging logic transcend"
    },
    {
      "section": "api",
      "id": "transcend.app.controller:ReportsDialogCtrl",
      "shortName": "ReportsDialogCtrl",
      "type": "controller",
      "moduleName": "transcend.app",
      "shortDescription": "The &#39;ReportsDialogCtrl&#39; controller controls the dialog window and ensures the correct profile information is set.",
      "keywords": "$modalinstance $scope api app appconfig controller controls correct dialog ensures profile reportsdialogctrl set transcend window"
    },
    {
      "section": "api",
      "id": "transcend.app.controller:ResetPasswordCtrl",
      "shortName": "ResetPasswordCtrl",
      "type": "controller",
      "moduleName": "transcend.app",
      "shortDescription": "The &#39;ResetPasswordCtrl&#39; controller controls the Reset Password dialog.",
      "keywords": "$modalinstance $notify $scope api app controller controls core dialog password reset resetpasswordctrl security service transcend"
    },
    {
      "section": "api",
      "id": "transcend.app.controller:RolesInfoCtrl",
      "shortName": "RolesInfoCtrl",
      "type": "controller",
      "moduleName": "transcend.app",
      "shortDescription": "The &#39;RolesInfoCtrl&#39; controller has information about the roles for users, and counts the number of Users that have",
      "keywords": "$privilege $rolemanager $scope api app controller counts number role roles rolesinfoctrl security specific transcend users"
    },
    {
      "section": "api",
      "id": "transcend.app.controller:SignInRequiredMsgCtrl",
      "shortName": "SignInRequiredMsgCtrl",
      "type": "controller",
      "moduleName": "transcend.app",
      "shortDescription": "The &#39;SignInRequiredMsgCtrl&#39; controller holds the logic responsible for notifying the user if a sign up is required.",
      "keywords": "$modalinstance $rolemanager $scope access api app control controller holds logic menu notifying required responsible security sign signinrequiredmsgctrl transcend user"
    },
    {
      "section": "api",
      "id": "transcend.app.controller:UserInfoCtrl",
      "shortName": "UserInfoCtrl",
      "type": "controller",
      "moduleName": "transcend.app",
      "shortDescription": "The &#39;UserInfoCtrl&#39; controller handles updating and changing information/permissions in User Accounts",
      "keywords": "$authenticate $notify $scope $window accounts api app changing controller core handles security transcend updating user userinfoctrl"
    },
    {
      "section": "api",
      "id": "transcend.app.directive:accessControlManagerMenu",
      "shortName": "accessControlManagerMenu",
      "type": "directive",
      "moduleName": "transcend.app",
      "shortDescription": "The &#39;accessControlManagerMenu&#39; directive creates a simple navigation menu item to show the &quot;access control manager&quot;",
      "keywords": "$httpbackend $rolemanager access access-control-manager-menu accesscontrolmanagermenu angular api app appconfig bar calls caret catalog class component connection connectionstring control controller copy creates ctrl data db default defaultconnection directive dropdown dropdown-menu dropdown-toggle ea enabled false foreach function glyphicon glyphicon-plus href html http info item js main-nav manager menu menuitem message mock mocks module myhost myoracledb mysqldb nav nav-pills navbar navbar-brand navbar-default navbar-nav navigation ng-controller ngmocke2e oracle oracleclient passed provider respond role script sdf security server service simple source sql sqlclient sqlserverce successful system transcend var whenget whenpost"
    },
    {
      "section": "api",
      "id": "transcend.app.directive:appProfileList",
      "shortName": "appProfileList",
      "type": "service",
      "moduleName": "transcend.app",
      "shortDescription": "Provides a simple list of profiles and provides the ability to add/delete them - if you have the privileges to.",
      "keywords": "$array $httpbackend $location $notify $rolemanager $showmodal $window _blank ability add add-roles analyzer angular api app appconfig appid application appname appprofile arcgis aspx basemapselector btn btn-lg btn-primary bypassproxy bypassqueryorderby calls carolina cartographic cdot check class colorado coloradodot common contrast controller copy core ctrl data dateselection dc ddot default delete-roles diagram directive docs dot driving errorreportemail esriconfig finished function ga gdot georgia gov guide happytown haslrsmeta hasnumericrouteid help homepageurl href html http iadot images implemented incomplete indiana indot info iowa iowadot js khaki links list logo lrs lrsprofileid map mapserver maryland maxquerylayers mdot mdsha message michigan midnight mock mocks modal module my-app navigating ncdot network ng-click ng-controller ngmocke2e north ocean odot ohio org patriot penndot place png privileges process profile profiles ra respond road roads roadway roles routelistorderby royal scale scenarios script service setloadedprofileasdefault settings signsystems simple spacelab straight sunrise superhero target theme time topo transcend transcendspatial transportation tss txdot url var vdot versionselection viewing virginia virginiadot visibility web-common west whenget wv wvdot"
    },
    {
      "section": "api",
      "id": "transcend.app.directive:dateRangePicker",
      "shortName": "dateRangePicker",
      "type": "directive",
      "moduleName": "transcend.app",
      "shortDescription": "The &#39;dateRangePicker&#39; directive provides the ability to create a datePicker that allows the user to pick",
      "keywords": "ability allows angular api app appconfig controller create ctrl datepicker daterangepicker directive ea from-date function html js module ng-controller pick script to-date transcend user"
    },
    {
      "section": "api",
      "id": "transcend.app.directive:dbConnectionsMenu",
      "shortName": "dbConnectionsMenu",
      "type": "directive",
      "moduleName": "transcend.app",
      "shortDescription": "The &#39;dbConnectionsMenu&#39; directive creates a simple navigation menu item to show the &quot;db connections manager&quot;",
      "keywords": "$httpbackend $rolemanager angular api app appconfig bar calls caret catalog class component connection connections connectionstring controller copy creates ctrl data db db-connections-menu dbconnectionsmenu default defaultconnection directive dropdown dropdown-menu dropdown-toggle ea enabled false foreach function glyphicon glyphicon-plus href html http info item js main-nav manager menu menuitem message mock mocks module myhost myoracledb mysqldb nav nav-pills navbar navbar-brand navbar-default navbar-nav navigation ng-controller ngmocke2e oracle oracleclient passed provider respond role script sdf security server service simple source sql sqlclient sqlserverce successful system transcend var whenget whenpost"
    },
    {
      "section": "api",
      "id": "transcend.app.directive:floatingPane",
      "shortName": "floatingPane",
      "type": "directive",
      "moduleName": "transcend.app",
      "shortDescription": "The &#39;floatingPane&#39; directive creates a floating window pane, which is a fixed container that can sit at any corner",
      "keywords": "api app appconfig automatically bookmarks bottom-left change container corner creates directive ea elements fixed floating floatingpane house menu pane position-class positioning relative screen sit size small todo transcend window"
    },
    {
      "section": "api",
      "id": "transcend.app.directive:helpMenu",
      "shortName": "helpMenu",
      "type": "directive",
      "moduleName": "transcend.app",
      "shortDescription": "The &#39;helpMenu&#39; directive provides a starting template for help Menus.",
      "keywords": "$log $rolemanager angular api app appconfig application bar builddate class configuring controller ctrl debug description directive ea enabled error false function help help-menu helpmenu html js log main-nav menus mocks module nav nav-pills navbar navbar-brand navbar-default navbar-nav navigation ng-controller ngmocke2e role script service start starting template transcend var version warn warning"
    },
    {
      "section": "api",
      "id": "transcend.app.directive:inlineAccountMgr",
      "shortName": "inlineAccountMgr",
      "type": "directive",
      "moduleName": "transcend.app",
      "shortDescription": "The &#39;inline account manager&#39; directive creates a simple wrapper for the",
      "keywords": "$authenticate $httpbackend $rolemanager $scope access_token account actions angular api app aug b6aad2d8-2fdd-4b07-91f9-ada394f1ee95 bearer calls changing class congratulations console controller creates credentials ctrl data directive ea email enabled enter error error_description expires expires_in false firstname forgot function gmt grant_type green header hooks html http including incorrect info inline invalid_grant issued js lastname log manager managing manual message method mock mocks module mon navbar navbar-default navigation ng-controller ng-style ngmocke2e nov on-fail on-success onfail onsuccess password recieved red rememberme respond rgreen role roles rusty script security service sign signed simple siteadmin style sun support test-token token_type transcend update url user username valid whenpost wrapper"
    },
    {
      "section": "api",
      "id": "transcend.app.directive:loaded",
      "shortName": "loaded",
      "type": "directive",
      "moduleName": "transcend.app",
      "shortDescription": "The &#39;loaded&#39; directive provides a the ability to bind (a hook) to whether an element loads or not. This is typically",
      "keywords": "ability api app bind css directive element failed file files flag fully hastheme hook item limited load loaded loadedfailed loadedsuccess loading loads ng-href ng-if rel represents scenario screen stylesheet theme themeloaded themes transcend typically window"
    },
    {
      "section": "api",
      "id": "transcend.app.directive:reportsMenu",
      "shortName": "reportsMenu",
      "type": "directive",
      "moduleName": "transcend.app",
      "shortDescription": "The &#39;reportsMenu&#39; directive creates a simple navigation menu item to show the &quot;reports list&quot; component.",
      "keywords": "$httpbackend $rolemanager angular api app appconfig bar barlegendimage boolean calls caret category class component controller copy creates ctrl description diagram directive double dropdown dropdown-menu dropdown-toggle ea enabled false filename frommeasure function glyphicon glyphicon-plus grayscale href html http image item js list main-nav menu menuitem mock mocks module nav nav-pills navbar navbar-brand navbar-default navbar-nav navigation ng-controller ngmocke2e pageheight pagewidth parameters profile ra repeatingimage report reports reports-menu reportsmenu respond role routeid scale script service signimage simple snapshot sticklegendimage straight string tomeasure transcend type var whenget"
    },
    {
      "section": "api",
      "id": "transcend.app.directive:resizeModal",
      "shortName": "resizeModal",
      "type": "directive",
      "moduleName": "transcend.app",
      "shortDescription": "The &#39;resizeModal&#39; directive allows the modal window to have a toggleable full screen option. The component is",
      "keywords": "$timeout allows api app button changed class clicked close component default determining directive ea expression fired flag full maximized modal modal-body modal-footer modal-header onresize option portion resizemodal screen size start text-center toggleable transcend typically valid values window"
    },
    {
      "section": "api",
      "id": "transcend.app.provider:$app",
      "shortName": "$app",
      "type": "service",
      "moduleName": "transcend.app",
      "shortDescription": "The &#39;$app&#39; factory provides functionality to bootstrap (initialize) and application. The intent is that the app",
      "keywords": "$app $appprovider afterprofileload aftersignin api app appconfig appid application autoconfigs beforeprofileload beforesignin behaviour bootstrap config configuration configure configuremodules datasourceconfig default defaultprofileflag dictates drawingconfig entity esriconfig exportconfig exposes factory file functionality hooks http intent jobconfig json lidarconfig load lrsconfig method my-app-id myconfiguremoduleshook myonresolvehook noop object onrejected process profile provider reportingconfig represents resolve rest securityconfig securitymanagerconfig service serviceurl string transcend tsconfig waitsfor"
    },
    {
      "section": "api",
      "id": "transcend.app.service:$showModal",
      "shortName": "$showModal",
      "type": "service",
      "moduleName": "transcend.app",
      "shortDescription": "The &#39;$showModal&#39; service provides a quick way to pop open a bootstrap modal, using default configuration. It also",
      "keywords": "$modal $showmodal addition allow api app appconfig backdrop bootstrap cancel clicking close complete configuration controller default false flexible function html keyboard manner methods modal mode myctrl object omit open parameter passing pop quick scope service strict template templateurl transcend true url web-common"
    },
    {
      "section": "api",
      "id": "transcend.app.service:AppProfile",
      "shortName": "AppProfile",
      "type": "service",
      "moduleName": "transcend.app",
      "shortDescription": "The &#39;AppProfile&#39; factory creates and returns a profile object containing configuration options",
      "keywords": "$resource api app appconfig appprofile configuration creates factory object options profile returns service transcend"
    },
    {
      "section": "api",
      "id": "transcend.controls.directive:dbConnectionNavbar",
      "shortName": "dbConnectionNavbar",
      "type": "directive",
      "moduleName": "transcend.controls",
      "shortDescription": "The &#39;dbConnectionNavbar&#39; provides an editable navigation bar template for the database connections menu.",
      "keywords": "api bar connections controls database dbconnectionnavbar directive ea editable menu navigation template transcend"
    },
    {
      "section": "api",
      "id": "transcend.controls.directive:lrsProfileNavbar",
      "shortName": "lrsProfileNavbar",
      "type": "directive",
      "moduleName": "transcend.controls",
      "shortDescription": "The &#39;lrsProfileNavbar&#39; provides an editable navigation bar template for the Lrs Profile menu",
      "keywords": "api bar controls directive ea editable lrs lrsprofilenavbar menu navigation profile template transcend"
    },
    {
      "section": "api",
      "id": "transcend.core",
      "shortName": "transcend.core",
      "type": "overview",
      "moduleName": "transcend.core",
      "shortDescription": "Core Module",
      "keywords": "$array $color $object $string add addition angular api application applications base basic bower build code commands common commonly components core dependency deprecate determine download easiest factory files functionality git goal goals https implement included install installation interfaces io js libraries library majority minified module modules moved mymodule nature object options org overview package permissions project provide pulled repository separate services set simple simply size small source transcend tsconfig tsstools utilitarian utils view weened zip"
    },
    {
      "section": "api",
      "id": "transcend.core.$array",
      "shortName": "transcend.core.$array",
      "type": "service",
      "moduleName": "transcend.core",
      "shortDescription": "Provides a set of &#39;array&#39; utility functions. These methods are &#39;static&#39; functions found on the",
      "keywords": "$array accept add addition api applicable applied applying args arguments array arrays ascending attribute attributes based build cache cacheditem call calls chaining check comparison concatenate configured containsall containsany converts core count create creates default descending destination determine determines dictionary element execute expect factory false field filter filtered filteron findbyid foo frommap function functions identifier input instance instantiable instantiated instantiating isarray item items key keyfield keyprop keyproperty length limit list map max maximum merge mergearrayspropertyidentifier merges method methods min minimum minus multi-level multiple named negative nested note null object objects operations order pair pairs pass passed perform pluck plucks point prefix primitive properties property propertyname provided reference retrieve retrieves return returns search select service set sets simple single-level skipped somemethod someothermethod sort sortbyproperty sorting source specific src static string three time tobeequal tobefalsy tobetruthy toequal tomap transcend true turned types undefined unique util utility value1a value1b value1c value2a value2b value2c value3a value3b value3c valuefield values var"
    },
    {
      "section": "api",
      "id": "transcend.core.$errorMsg",
      "shortName": "transcend.core.$errorMsg",
      "type": "service",
      "moduleName": "transcend.core",
      "shortDescription": "Provides a simple method to extract/find an error message from an error/exception object.",
      "keywords": "$errormsg api core deep error exception exceptionmessage expect extract find level message method msg nested object service simple single string toequal transcend"
    },
    {
      "section": "api",
      "id": "transcend.core.$loading",
      "shortName": "transcend.core.$loading",
      "type": "service",
      "moduleName": "transcend.core",
      "shortDescription": "Provides a set of utility functions for use while the application is loading data. These methods are &#39;static&#39;",
      "keywords": "$loading allowed api application assume based call calls check controlled core current currently currentsecuritykey data description determine display equal expect factory fail false functions greater idle increment indicating key loading locked lockeddown maximum message message1 method methods non-null notcurrentsecuritykey number object operation passed percentage point proceed process progress resetting returned returns rules running saved security securitykey service session set somemethod start startat static status stops succeed ticks toequal total totalticks transcend true update updating user utility valid values"
    },
    {
      "section": "api",
      "id": "transcend.core.$notify",
      "shortName": "transcend.core.$notify",
      "type": "service",
      "moduleName": "transcend.core",
      "shortDescription": "the &quot;$notify&quot; factory provides functionality for the notify system to help log errors and other important things",
      "keywords": "$errormsg $log $notify $promise $scope $string ability add amount api app application automatically based behaviour catch change changed changepassword children common communicate components concept congrats core decorator default determine display error errormsg errors exception extended factory failed false feedback finally flag functionality generic help indicates individual infinite inner instance instances interface item log logclosed logged logs maximum message messages method model msg notification notify object occur occurs ooops password profile promise property provide rejected rejection replaced resolved returned save saved saving service single skiptimetrack spinner start success system things time title toaster track tracking transcend true tsconfig typically update user wait waiting waittime working"
    },
    {
      "section": "api",
      "id": "transcend.core.$object",
      "shortName": "transcend.core.$object",
      "type": "service",
      "moduleName": "transcend.core",
      "shortDescription": "Provides a set of &#39;object&#39; utility functions. These methods are &#39;static&#39; functions found on the",
      "keywords": "$object $protofactory add addition api arenumbers args argument arguments attribute attributes bar based blah cache cached cacheditem call calls center check configuration copying core definition destination determine determines dst entire example execute expect extend extended extends factory fails foo function functions haskeys instance instantiable instantiated item key lon longitude main map match matches merge merges method methods multi-level multiple note number numbers obj object objects operations options opts overwrite overwriting overwritten pass passed perform point potential properties property reference representation retrieve return returned returns scenario service set sets settervalue setvalue simple somemethod someothermethod source source1 source2 src static string test tests time tobefalsy tobetruthy toequal transcend traverse traversed traverses true types undefined utility var wanted"
    },
    {
      "section": "api",
      "id": "transcend.core.$protoFactory",
      "shortName": "transcend.core.$protoFactory",
      "type": "service",
      "moduleName": "transcend.core",
      "shortDescription": "the &#39;$protoFactory&#39; factory adds prototype methods to classes.",
      "keywords": "$protofactory adds api classes core factory methods prototype service transcend"
    },
    {
      "section": "api",
      "id": "transcend.core.$string",
      "shortName": "transcend.core.$string",
      "type": "service",
      "moduleName": "transcend.core",
      "shortDescription": "Provides a set of &#39;string&#39; utility functions. These methods are &#39;static&#39; functions found on the",
      "keywords": "$string actions addition api applied applies apply args argument arguments based cache cached cacheditem call calls capitalizing character combined core example execute expect factory format formatted formattedstring formatting function functions hello helloworld indexed input instance instantiable instantiated item main method methods multiple note object operations original pass passed perform point prettify purpose readable reference replace replaced retrieve separating service set somemethod someothermethod source static str string strings text time toequal tokens transcend turns utility var world"
    },
    {
      "section": "api",
      "id": "transcend.core.directive:searchBox",
      "shortName": "searchBox",
      "type": "service",
      "moduleName": "transcend.core",
      "shortDescription": "The &#39;searchBox&#39; directive provides a compact search input box.",
      "keywords": "$timeout angular api app billy bind black blow box compact controller core ctrl directive doe filter function html input jack job joe joel john js list mae module newvalue ng-controller ng-repeat ngmocke2e on-change onchange people person persons sally script search searchbox searched service sometext text transcend tsconfig"
    },
    {
      "section": "api",
      "id": "transcend.core.inArray",
      "shortName": "transcend.core.inArray",
      "type": "filter",
      "moduleName": "transcend.core",
      "shortDescription": "Provides the ability to filter an array using another array",
      "keywords": "$filter $scope ability angular api app array controller core ctrl element filter filterby function html inarray js letter letters list match module ng-controller ng-repeat script transcend values"
    },
    {
      "section": "api",
      "id": "transcend.core.pretty",
      "shortName": "transcend.core.pretty",
      "type": "filter",
      "moduleName": "transcend.core",
      "shortDescription": "Converts a string to an alternative more &quot;readable&quot; format.",
      "keywords": "$scope $string alternative angular api app controller converts core ctrl filter format function html is_somesuperugly_text js module mytext ng-controller ng-model pretty readable script string text transcend"
    },
    {
      "section": "api",
      "id": "transcend.data-source",
      "shortName": "transcend.data-source",
      "type": "overview",
      "moduleName": "transcend.data-source",
      "shortDescription": "Data Source Module",
      "keywords": "add angular api application bower code commands components connections create data data-source database delete dependency download easiest files git goals https implement included install installation io js libraries manage minified module mymodule options org overview package permissions project pulled repository represent simply size source sources tests transcend tsstools unit update view zip"
    },
    {
      "section": "api",
      "id": "transcend.data-source.directive:dbConnectionEditor",
      "shortName": "dbConnectionEditor",
      "type": "directive",
      "moduleName": "transcend.data-source",
      "shortDescription": "The &#39;dBConnectionEditor&#39; directive provides a template for a menu to edit the database connections.",
      "keywords": "$connectiontest $notify $resourcelistcontroller api connections core data-source database datasourceproviders dbconnectioneditor directive ea edit menu template transcend"
    },
    {
      "section": "api",
      "id": "transcend.data-source.directive:dbConnectionList",
      "shortName": "dbConnectionList",
      "type": "directive",
      "moduleName": "transcend.data-source",
      "shortDescription": "The &#39;dbConnectionList&#39; service",
      "keywords": "$array $connectiontest $httpbackend $notify $resourcelistcontroller access allow angular api app begun bind calls catalog completed connection connectionstring controller copy ctrl data data-source datasource datasourceproviders db dbconnectionlist default defaultconnection determines directive editable editing failed fired foreach function functionality gain html http info js list message mock mocks module myhost myoracledb mysqldb ng-controller ngmocke2e object onsave onsavebegin onsaveerror oracle oracleclient order passed process provider respond save script sdf security server service source sources sql sqlclient sqlserverce successful system transcend transcend-core transcend-resource var whenget whenpost"
    },
    {
      "section": "api",
      "id": "transcend.data-source.service:$connectionTest",
      "shortName": "$connectionTest",
      "type": "service",
      "moduleName": "transcend.data-source",
      "shortDescription": "The &#39;$connectionTest&#39; factory provides the ability to test database connections",
      "keywords": "$connectiontest $notify ability api connection connections contianing core data-source database datasource factory failure info method object returns service success test tests transcend valid"
    },
    {
      "section": "api",
      "id": "transcend.data-source.service:DataSource",
      "shortName": "DataSource",
      "type": "service",
      "moduleName": "transcend.data-source",
      "shortDescription": "A $resource to interact with a &quot;DataSource&quot; entity.",
      "keywords": "$resource angularjs api connection connectionstring data data-source database datasource datasourceconfig entity http interact list models myconnection org query result service single source sources string test testconnection testconnectionbyid transcend var"
    },
    {
      "section": "api",
      "id": "transcend.diagnostics",
      "shortName": "transcend.diagnostics",
      "type": "overview",
      "moduleName": "transcend.diagnostics",
      "shortDescription": "Diagnostics Module",
      "keywords": "$log add angular angularjs api application bower calculations code commands component debugging default defaults dependency diagnostics directive download easiest elapsed extends files functionality git hierarchical https included install installation io isolated js libraries logs modify module move mymodule operations options org overview package permissions primary provide pulled refactor repository scope service set simply source support time tracking transcend tree treelog tsstools view zip"
    },
    {
      "section": "api",
      "id": "transcend.diagnostics.$log",
      "shortName": "transcend.diagnostics.$log",
      "type": "service",
      "moduleName": "transcend.diagnostics",
      "shortDescription": "Extends the default AngularJS $log service to provide a hierarchical",
      "keywords": "$log $q angularjs api array asynchronous automatically based cache calculate calculations catch child children clear clears complete debug default defer diagnostics elapsed error extends happen hierarchical http info linear log logged logs marked message method nested object operations org process promise provide representation resolve running service start starts stops time tracking transcend turns var warn"
    },
    {
      "section": "api",
      "id": "transcend.diagnostics.$time",
      "shortName": "transcend.diagnostics.$time",
      "type": "filter",
      "moduleName": "transcend.diagnostics",
      "shortDescription": "Provides an elapsed filter that:",
      "keywords": "$filter $time alias api converts diagnostics difference elapsed elapsedfilter equivalent example filter finish format formatted mapping method metric millis milliseconds ms returns seconds start suffixmap takes time transcend units values var"
    },
    {
      "section": "api",
      "id": "transcend.diagnostics.directive:logTree",
      "shortName": "logTree",
      "type": "directive",
      "moduleName": "transcend.diagnostics",
      "shortDescription": "The &#39;logTree&#39; directive provides a simple interactive tree structure to view your nested logs.",
      "keywords": "$compile $log $scope angular api app application btn btn-primary built class controller create ctrl debug defaults diagnostics directive error expose failed filter function html info initial interactive js loading log log-tree logmsg logs logtree logtype message module nested ng-click ng-controller ng-model object render rendered script search service set simple start start1 start2 structure test text transcend tree tree-model treemodel type var view views warn"
    },
    {
      "section": "api",
      "id": "transcend.diagnostics.elapsed",
      "shortName": "transcend.diagnostics.elapsed",
      "type": "filter",
      "moduleName": "transcend.diagnostics",
      "shortDescription": "Provides the ability to show elapsed time in human readable text. If a second parameter &quot;end date&quot; is not passed in,",
      "keywords": "$filter $time ability alias api current day diagnostics elapsed elapsedfilter example expect filter hr human mapping millis min ms parameter passed readable second seconds start suffixmap supplement text time toequal transcend units var"
    },
    {
      "section": "api",
      "id": "transcend.diagnostics.time",
      "shortName": "transcend.diagnostics.time",
      "type": "filter",
      "moduleName": "transcend.diagnostics",
      "shortDescription": "Provides a filter to show milliseconds in human readable text.",
      "keywords": "$filter $time api diagnostics elapsed expect filter human milliseconds ms readable text time timefilter toequal transcend var"
    },
    {
      "section": "api",
      "id": "transcend.drawing",
      "shortName": "transcend.drawing",
      "type": "overview",
      "moduleName": "transcend.drawing",
      "shortDescription": "Drawing Module",
      "keywords": "add angular api application better bower capabilities code commands components container dependency download drawing easiest explore files git goals https include included install installation io js libraries lines minified module mymodule options org overview package permissions points project pulled rectangles repository simply size solution source svg tests text transcend tsstools unit view zip"
    },
    {
      "section": "api",
      "id": "transcend.drawing.directive:drawToolbar",
      "shortName": "drawToolbar",
      "type": "directive",
      "moduleName": "transcend.drawing",
      "shortDescription": "The &#39;drawToolbar&#39; directive provides an editable toolbar for the SVG Canvas.",
      "keywords": "$svgcanvasgroup api canvas directive drawing drawingconfig drawtoolbar ea editable svg toolbar transcend"
    },
    {
      "section": "api",
      "id": "transcend.drawing.directive:svgCanvas",
      "shortName": "svgCanvas",
      "type": "directive",
      "moduleName": "transcend.drawing",
      "shortDescription": "The &#39;svgCanvas&#39; directive provides a template for a Scalable Vectory Graphics (SvG) Canvas.",
      "keywords": "$color $rootscope $svgcanvasgroup $timeout $vectoreditor $window api canvas directive drawing ea graphics scalable settings svgcanvas template transcend vectory"
    },
    {
      "section": "api",
      "id": "transcend.drawing.service:$svgCanvasGroup",
      "shortName": "$svgCanvasGroup",
      "type": "service",
      "moduleName": "transcend.drawing",
      "shortDescription": "The &#39;$svgCanvasGroup&#39; factory adds properties to the SVG Canvas.",
      "keywords": "$svgcanvasgroup $window adds api canvas drawing factory properties service svg transcend"
    },
    {
      "section": "api",
      "id": "transcend.drawing.service:$vectorEditor",
      "shortName": "$vectorEditor",
      "type": "service",
      "moduleName": "transcend.drawing",
      "shortDescription": "The &#39;$vectorEditor&#39; factory returns a new Vector Editor for SVG purposees.",
      "keywords": "$vectoreditor $window api drawing editor factory purposees returns service svg transcend vector"
    },
    {
      "section": "api",
      "id": "transcend.dynamic-grid",
      "shortName": "transcend.dynamic-grid",
      "type": "overview",
      "moduleName": "transcend.dynamic-grid",
      "shortDescription": "LRS Grid Editor Module",
      "keywords": "add allows angular angularui api application bower code commands complete component components coverage custom data define dependency difference directly download dynamic-grid easiest editing editor edits esri files functionality git grid history https included install installation instance io js libraries lrs lrs-grid-editor map module mymodule options org overview package process pulled rce records repository simply standard test tools transcend tsstools unit view zip"
    },
    {
      "section": "api",
      "id": "transcend.dynamic-grid.directive:dynamicGridContainer",
      "shortName": "dynamicGridContainer",
      "type": "directive",
      "moduleName": "transcend.dynamic-grid",
      "shortDescription": "The &#39;dynamicGridContainer&#39; provides a grid container template.",
      "keywords": "$compile api container directive dynamic-grid dynamicgridcontainer grid template transcend"
    },
    {
      "section": "api",
      "id": "transcend.dynamic-grid.directive:dynamicGridRow",
      "shortName": "dynamicGridRow",
      "type": "directive",
      "moduleName": "transcend.dynamic-grid",
      "shortDescription": "The &#39;dynamicGridRow&#39; directive provides an editable template for a grid row.",
      "keywords": "$document api directive dynamic-grid dynamicgridconfig dynamicgridrow editable grid row template transcend"
    },
    {
      "section": "api",
      "id": "transcend.dynamic-grid.dynamicGrid",
      "shortName": "transcend.dynamic-grid.dynamicGrid",
      "type": "directive",
      "moduleName": "transcend.dynamic-grid",
      "shortDescription": "The &#39;dynamicGrid&#39; directive provides a flexible, easy to use data grid that integrates with transcend.resource.$metaData.",
      "keywords": "$compile $metadata $scope $string action advanced age akushon alert alice allow allowing alwaysedit angular api app applied argument array assume automatically bare basic behavior behaviors bones boolean button call called case cells checkbox collection column column1 column2 columns compiled component components configuration content controller core covers ctrl data datum default define defined defines definition described descriptor descriptors desired directive disallowed drooling dynamic-grid dynamicgrid easy edit editable editing editor empty english example expected expects extended extends flexible format formatted function ga generated georgia greethtml greeting grid grid-specific griddata0 griddata1 griddata2 gridmetadata1 gridmetadata2 hello heretofore highlighted hobby html human-readable ice injection input instance integrates item items javascript js label language left link linked lower member metadata metadata-config metadataconfig metadatum modifies module named names ng-click ng-controller note number object objects optional order outlined overridden pa parameters passed passing pennsylvania plain postalcode preprocessing properly properties property provided push readonly rendering renders resource return richard row scope script service shared simple simply skating sortable sorting special static string style subsequent support tag test transcend transcluded true type ui-scope uiscope usage values var viewer visualize visualized"
    },
    {
      "section": "api",
      "id": "transcend.esri",
      "shortName": "transcend.esri",
      "type": "overview",
      "moduleName": "transcend.esri",
      "shortDescription": "ESRI Module",
      "keywords": "$symbol add allows angular angularjs api application apply arcgis attributes automatically based benefit binding bower center code color commands component components create dependency designated dojo download easiest easily esri feature files find geometry git https included install installation integration io javascript js layer layers level libraries list location main map method mew mock module mymodule options org overview package polyline pulled remain repository returns reusable service set share simply symbol sync tests transcend tsstools two-way type unit values var view zip zoom"
    },
    {
      "section": "api",
      "id": "transcend.esri.$arcgis",
      "shortName": "transcend.esri.$arcgis",
      "type": "service",
      "moduleName": "transcend.esri",
      "shortDescription": "Provides a set of utility functions related to the ArcGIS JavaScript API - particularly",
      "keywords": "$arcgis $arcgisprovider $geometry $log $object $q $rootscope $symbol abc action actions add addition additional addlayers adds addselection allow angular angularjs api arcgis arcgisdynamicmapservicelayer arcgisonline args argument array assuming auto automatically based behaviour blue cache cachedmap call callback called center chaining class clear cleargraphics clears click color comma common complete completes components config configs configuration configured configuring conversion convert create created creates creating custom default defaultextension defer depending details determine dojo dom don draw draws endpoint esri esriconfig esrigraphic event-click exist existing exists expandby expect export extend extension extent extracts factor factory false falsy feature file filename fire flag function functionality functions geometry geometryurl graphic graphiclayer graphiclayerid graphicobject graphics graphicslayer handler highlight html http https identify identifyparameters ids instance instances javascript json keepvisible lat layer layer_option_visible layerid layerid2 layerids layeroption layerorurl layers layersconfig layerurl list location lon main map mappoint mercator message messages method methods missing module mouseevent mozilla myapp newcenter object objects omitcreation onbeforeaddedtomap onclick onidentify online operations options opts org override parameter pass passed perform performing performs place point project projection projects promise properties property provide provider purpose reference refers remove representation resolve resolved rest retrieve retrieved retrieves return returned returngeometry returnm returns safe-guard screenpoint selected selection sepearted server servers service set sets setter setting simply somelayerid somemethod someothermethod spatial spatialreference specific sptial standard static stored string symbol symbology task tasks time toequal tolerance transcend true turn turned type url util utility valid valuenoattributes var viewport web wkid work zoom zooms"
    },
    {
      "section": "api",
      "id": "transcend.esri.$layer",
      "shortName": "transcend.esri.$layer",
      "type": "service",
      "moduleName": "transcend.esri",
      "shortDescription": "Provides a set of utility functions related to a plain layer object (not an instantiated object).",
      "keywords": "$array $layer $protofactory alias api args based cache code coded common core defaultfield displayfield domain domains domainvalue esri expect field field1 field2 fieldname fields fieldvalue functionality functions instantiated layer lookup lyr main matched method object operations original parameter pass passed perform performed plain property provide purpose reference returned service set somemethod someothermethod static stored toequal transcend undefined utility var"
    },
    {
      "section": "api",
      "id": "transcend.esri.directive:basemapSelector",
      "shortName": "basemapSelector",
      "type": "directive",
      "moduleName": "transcend.esri",
      "shortDescription": "The &#39;basemapSelector&#39; directive displays a list of available basemaps for the current application session and",
      "keywords": "$scope active angular api app application arcgis basemap basemaps basemapselector bm bottom center change changes clicked controller css ctrl currenly current directive display displays ea esri esriconfig function gray href html http hybrid js left list map method module national-geographic ng-controller ng-model ng-options oceans onchange options osm position rel satellite script selected selection session streets style stylesheet topo transcend true valid zoom"
    },
    {
      "section": "api",
      "id": "transcend.esri.directive:esriMap",
      "shortName": "esriMap",
      "type": "directive",
      "moduleName": "transcend.esri",
      "shortDescription": "The &#39;esriMap&#39; directive allows the creation of ESRI maps using the ArcGIS JavaScript API in markup and",
      "keywords": "$arcgis $q $scope $timeout $window allows angular api app arcgis area argument attribute basemap bound cache cacheevent cachekey callback center change changes configuration content controller coordinate core correspond create creation css ctrl currently directive dojo ea empty esri esriconfig esrimap event extent fields files fired flag function gray guarantee guarantees height href html hybrid ignored include infos initially instance instantiated javascript js key lat latitude lattitude layer layers level ln lng loaded loading location lon longitude lt map maps markup module national-geographic ng-controller ng-model note null number object oceans onbeforelayersadded onlayersready onready osm parameter pass passed persist populated property ready rel requires satellite save script session sessions set setup start step store streets string stylesheet supports things todo topo transcend type user ways width zoom zoomtolayer"
    },
    {
      "section": "api",
      "id": "transcend.esri.directive:featureSelection",
      "shortName": "featureSelection",
      "type": "directive",
      "moduleName": "transcend.esri",
      "shortDescription": "The &#39;featureSelection&#39; directive provides a subset of tools to enable the selection of features for the specified map layers and enables",
      "keywords": "$arcgis $array $filter $notify $scope $timeout active angular api app arcgis basemap basemaps bm bottom center change controller core css ctrl current directive display dojo ea enable enables esri esriconfig features featureselection function gray highlighting href html http hybrid js layers left map method module national-geographic ng-controller ng-model ng-options oceans onchange options osm position rel satellite script selected selection session streets style stylesheet subset tools topo transcend true valid zoom"
    },
    {
      "section": "api",
      "id": "transcend.esri.directive:hierarchyFilter",
      "shortName": "hierarchyFilter",
      "type": "directive",
      "moduleName": "transcend.esri",
      "shortDescription": "The &#39;hierarchyFilter&#39; directive provides an editable template for the Search By Hierarchy Tool",
      "keywords": "$filter $http $layer $loading $notify $timeout api core directive ea editable esri hierarchy hierarchyfilter mapserver search template tool transcend tsconfig"
    },
    {
      "section": "api",
      "id": "transcend.esri.esriConfig",
      "shortName": "transcend.esri.esriConfig",
      "type": "object",
      "moduleName": "transcend.esri",
      "shortDescription": "Provides a single configuration object that can be used to change the behaviour, options, urls, etc for any",
      "keywords": "angular angularjs api basemapselector behaviour change components configuration esri esriconfig http map module myapp object options org overridden override single streets transcend urls"
    },
    {
      "section": "api",
      "id": "transcend.esri.factory:$date",
      "shortName": "$date",
      "type": "service",
      "moduleName": "transcend.esri",
      "shortDescription": "The &#39;$date&#39; factory provides multiple methods of modifying the Date into the necessary format for querying or display.",
      "keywords": "$date $filter $string $todate $tomilliseconds api changes core creates datevalue display esri factory flag format formatted isnumericstring method methods milliseconds modified modifying multiple number numeric object querying service string tests time transcend"
    },
    {
      "section": "api",
      "id": "transcend.esri.filter:featureOrderBy",
      "shortName": "featureOrderBy",
      "type": "filter",
      "moduleName": "transcend.esri",
      "shortDescription": "The &#39;featureOrderBy&#39; filter orders a given feature set by a selected feature.",
      "keywords": "api esri feature featureorderby filter orders selected set transcend"
    },
    {
      "section": "api",
      "id": "transcend.esri.filter:utcDate",
      "shortName": "utcDate",
      "type": "filter",
      "moduleName": "transcend.esri",
      "shortDescription": "The &#39;utcDate&#39; filter modifies a given date into the UTC format.",
      "keywords": "$date $filter api esri filter format modifies transcend utc utcdate"
    },
    {
      "section": "api",
      "id": "transcend.esri.lrsConfig",
      "shortName": "transcend.esri.lrsConfig",
      "type": "object",
      "moduleName": "transcend.esri",
      "shortDescription": "Provides a single configuration object that can be used to change the behaviour, options, urls, etc for any",
      "keywords": "angular angularjs api behaviour change components configuration esri esriconfig http lrs lrsconfig lrsprofile module myapp object options org overridden override single transcend url urls"
    },
    {
      "section": "api",
      "id": "transcend.esri.provider:MapServer",
      "shortName": "MapServer",
      "type": "service",
      "moduleName": "transcend.esri",
      "shortDescription": "The &#39;MapServer&#39; provider",
      "keywords": "api esri mapserver provider service transcend"
    },
    {
      "section": "api",
      "id": "transcend.esri.provider:QueryDefinition",
      "shortName": "QueryDefinition",
      "type": "service",
      "moduleName": "transcend.esri",
      "shortDescription": "The &#39;QueryDefinition&#39; service",
      "keywords": "$resource api esri esriconfig provider querydefinition service transcend"
    },
    {
      "section": "api",
      "id": "transcend.esri.service:$geometry",
      "shortName": "$geometry",
      "type": "service",
      "moduleName": "transcend.esri",
      "shortDescription": "Provides a set of geometry utility functions.  The purpose of this service is to provide the necessary geometry",
      "keywords": "$filter $geometry $object $resource angularjs api applies apply appropriate array attribute based checking concatenates convert converted corresponding create created creates criteria data default details determine determines distance documentation empty equal esri exists expect extent extract extractmeasures extracts false featuer feature find flattened force forcecreate functionality functions geom geometric geometries geometry handle http https interacting ispoint ispolygon ispolyline isspatialreference item javascript joinparts lat layer lon map mappoint mappoint1 mappoint2 mappoint3 mappoint4 matched measure method methods natively note notequal numbers object org parameter passed point point1 point2 pointfrommeasure points polygon polyline precision provide pt2 pt3 purpose reference remainder resource retrieve retrieves return returned returns route service set simply spatial spatialreference static string test tobefalsy tobetruthy transcend true undefined utility valid values var vertices wkid xattr xattrvalue yattr yattrvalue"
    },
    {
      "section": "api",
      "id": "transcend.esri.service:$symbol",
      "shortName": "$symbol",
      "type": "service",
      "moduleName": "transcend.esri",
      "shortDescription": "Provides a quick and abstract method of creating ESRI symbology. The main use case for this service is",
      "keywords": "$resource $symbol abstract angularjs api arcgis call case class colors configuration create creating default details documentation dojo esri functions generate http include instances javascript main method methods natively note org point pointsize polygon polygonoutlinewidth polyline polylinecolor quick recreate resource service set simply static symbol symbology time transcend utility var"
    },
    {
      "section": "api",
      "id": "transcend.esri.service:DrawingInfo",
      "shortName": "DrawingInfo",
      "type": "service",
      "moduleName": "transcend.esri",
      "shortDescription": "The &#39;DrawingInfo&#39; factory returns methods",
      "keywords": "$class $color $string api core drawinginfo esri factory methods returns service transcend"
    },
    {
      "section": "api",
      "id": "transcend.esri:$$layer",
      "shortName": "$$layer",
      "type": "service",
      "moduleName": "transcend",
      "shortDescription": "The &#39;$$layer&#39; factory",
      "keywords": "$array $protofactory api esri factory service transcend"
    },
    {
      "section": "api",
      "id": "transcend.export",
      "shortName": "transcend.export",
      "type": "overview",
      "moduleName": "transcend.export",
      "shortDescription": "Export Module",
      "keywords": "add additional angular api application bower code commands csv currently data dependency download easiest export files formats functionality future git https implement included install installation io js libraries module mymodule options org overview package pulled repository set simply transcend tsstools type types view zip"
    },
    {
      "section": "api",
      "id": "transcend.export",
      "shortName": "transcend.export",
      "type": "overview",
      "moduleName": "transcend.export",
      "shortDescription": "Job Module",
      "keywords": "add angular api application bower change code commands data dependency download easiest expiration export files functionality git https images implement included install installation interact io job jobs js libraries long module mymodule options org overview package process processes pulled repository running set simply transcend tsstools typically view zip"
    },
    {
      "section": "api",
      "id": "transcend.export.$csv",
      "shortName": "transcend.export.$csv",
      "type": "service",
      "moduleName": "transcend.export",
      "shortDescription": "Provides a set of utility functions related to csv conversion. These methods are &#39;static&#39; functions found",
      "keywords": "$csv api arrays boolean buildcsvstring character comma complex conditions conversion convert create csv data def ends expect export factory false format formatted function functions key-value list meets method methods modified newline null number object objects operate original pairs passed replace replaced representation return returned returns service set somemethod static str string stringify stringify2 swaplastcommafornewline toequal transcend true type utility values"
    },
    {
      "section": "api",
      "id": "transcend.export.$download",
      "shortName": "transcend.export.$download",
      "type": "service",
      "moduleName": "transcend.export",
      "shortDescription": "The &#39;$download&#39; service provides the ability to download content. It is specifically useful to initiate a download",
      "keywords": "$document $download $log $string ability add api auto-detect bar based blank browsers client code content core csv data default defaultextension directly disposition doesn download downloads essentially export extension extract file filename fileorext finds foo form format header include initiate initiating json left major media media-type mediatype method my-file object optional passed populated post problem representation response scenes send serialized service side string submit submitting text transcend txt type upload wil"
    },
    {
      "section": "api",
      "id": "transcend.export.exportConfig",
      "shortName": "transcend.export.exportConfig",
      "type": "object",
      "moduleName": "transcend.export",
      "shortDescription": "Provides a single configuration object that can be used to change the behaviour, options, urls, etc for any",
      "keywords": "$download __params__ __url__ angular angularjs api backend base behaviour call capability change common components config configuration configurations configure default define download export exportconfig extend http initially module myapp object options org overridden override parameters passed properties represents resource resources server service set single specific string transcend ultimately url urls vary"
    },
    {
      "section": "api",
      "id": "transcend.job.directive:jobList",
      "shortName": "jobList",
      "type": "service",
      "moduleName": "transcend.job",
      "shortDescription": "The &#39;jobList&#39; directive allows the viewing, and editing of jobs.",
      "keywords": "$filter $httpbackend $interval $notify $q $timeout a66e65f0-9867-4aa0-9b2a-8f79bcd5686a access action advanced allows analysis analyze angular api app appconfig appid autorefresh basic bind bootstrap bulk calls cancelled changes column columns columsn comma comment comments complete console controller copy core ctrl default description details determines directive editing elapsedtime enddate errormessage event events exactly expirationdate expires failed false filepath function functionality gain hide hidecolumns html http ids ignoreowner invalid job jobconfig joblist jobs js layers length list log lrse_access mapping merge message mock mocks module names neverexpires ng-controller ngmocke2e object on-query onquery operator order owner ownerusername periodically poll profile profileid progress queried query query-params querying recordcount respond route routes running sa script sdf selected separated server service setting siteadmin startdate status tags test transcend true tss ui var viewing visibilities visibility whenget"
    },
    {
      "section": "api",
      "id": "transcend.job.service:Job",
      "shortName": "Job",
      "type": "service",
      "moduleName": "transcend.job",
      "shortDescription": "The &#39;Job&#39; directive provides methods to set up reporting Jobs.",
      "keywords": "$resource $window api directive job jobconfig jobs methods reporting service set transcend"
    },
    {
      "section": "api",
      "id": "transcend.lidar",
      "shortName": "transcend.lidar",
      "type": "overview",
      "moduleName": "transcend.lidar",
      "shortDescription": "Lidar Module",
      "keywords": "ability add angular api application bower code collected commands components data dependency download easiest files git goals hardware https included install installation interact io js libraries lidar minified module mymodule options org overview package permissions point project pulled query remote repository sensing simply size source tests transcend tsstools unit view viewer zip"
    },
    {
      "section": "api",
      "id": "transcend.lidar.directive:lidarViewer",
      "shortName": "lidarViewer",
      "type": "directive",
      "moduleName": "transcend.lidar",
      "shortDescription": "The &#39;lidarViewer&#39; directive provides an editable template for Lidar data.",
      "keywords": "$lidar $math $notify $q $window api core data directive ea editable lidar lidarconfig lidardata lidarviewer template transcend"
    },
    {
      "section": "api",
      "id": "transcend.lidar.service:$lidar",
      "shortName": "$lidar",
      "type": "service",
      "moduleName": "transcend.lidar",
      "shortDescription": "The &#39;$lidar&#39; service contains functions for manipulation of lidar collected points.",
      "keywords": "$lidar $math $window api collected functions lidar manipulation points service transcend"
    },
    {
      "section": "api",
      "id": "transcend.lidar:LidarData",
      "shortName": "LidarData",
      "type": "service",
      "moduleName": "transcend",
      "shortDescription": "The &#39;LidarData&#39; factory returns different configuration parameter options for Lidar Data",
      "keywords": "$resource api configuration data factory lidar lidarconfig lidardata options parameter returns service transcend"
    },
    {
      "section": "api",
      "id": "transcend.lrs",
      "shortName": "transcend.lrs",
      "type": "overview",
      "moduleName": "transcend.lrs",
      "shortDescription": "LRS Module",
      "keywords": "add angular api application bower code commands complete components coverage data dealing dependency download easiest files git https included install installation io js libraries lrs module mymodule options org overview package pulled repository reusable set simply test transcend tsstools unit view zip"
    },
    {
      "section": "api",
      "id": "transcend.lrs-data-grid.directive:transformLrsData",
      "shortName": "transformLrsData",
      "type": "service",
      "moduleName": "transcend.lrs-data-grid",
      "shortDescription": "The &#39;transformLrsData&#39; factory provides methods for transforming LRS data into the dynamic-grid format.",
      "keywords": "$string additional api applied attributes check core data directive dynamic-grid esri esridata factory flag format injections lrs lrs-data-grid methods raw service transcend transforming transformlrsdata transformlrsdataconfig"
    },
    {
      "section": "api",
      "id": "transcend.lrs-data-grid.lrsDataGrid",
      "shortName": "transcend.lrs-data-grid.lrsDataGrid",
      "type": "directive",
      "moduleName": "transcend.lrs-data-grid",
      "shortDescription": "A wrapper for transcend.dynamic-grid.dynamicGrid, specifically for visualizing data return from esri",
      "keywords": "$window api arcgis calls controls data directive dynamic-grid dynamicgrid esri esridata future history js keeping lrs-data-grid lrsdatagrid lrsdatagridconfig map mapserver methodology pulled query rce retire return showcontrols support transcend transformlrsdata visualize visualizing wrapper"
    },
    {
      "section": "api",
      "id": "transcend.lrs-data-grid.lrsGridEditorConfig",
      "shortName": "transcend.lrs-data-grid.lrsGridEditorConfig",
      "type": "object",
      "moduleName": "transcend.lrs-data-grid",
      "shortDescription": "Provides a single configuration object that can be used to change the behaviour, options, urls, etc for any",
      "keywords": "angular angularjs api behaviour change components configuration esriconfig http lrs lrs-data-grid lrsconfig lrsgrideditorconfig lrsprofile module myapp object options org overridden override single transcend url urls"
    },
    {
      "section": "api",
      "id": "transcend.lrs-data-grid.lrsGridEditorConfig",
      "shortName": "transcend.lrs-data-grid.lrsGridEditorConfig",
      "type": "object",
      "moduleName": "transcend.lrs-data-grid",
      "shortDescription": "Provides a single configuration object that can be used to change the behaviour, options, urls, etc for any",
      "keywords": "angular angularjs api behaviour change components configuration esriconfig http lrs lrs-data-grid lrsconfig lrsgrideditorconfig lrsprofile module myapp object options org overridden override single transcend url urls"
    },
    {
      "section": "api",
      "id": "transcend.lrs-data-grit.filter:resolvedFilter",
      "shortName": "resolvedFilter",
      "type": "filter",
      "moduleName": "transcend.lrs-data-grit",
      "shortDescription": "The &#39;resolvedFilter&#39;",
      "keywords": "api filter lrs-data-grit resolvedfilter transcend"
    },
    {
      "section": "api",
      "id": "transcend.lrs-grid-editor",
      "shortName": "transcend.lrs-grid-editor",
      "type": "overview",
      "moduleName": "transcend.lrs-grid-editor",
      "shortDescription": "LRS Grid Editor Module",
      "keywords": "add allows angular angularui api application bower code commands complete component components coverage custom data define dependency difference directly download easiest editing editor edits esri files functionality git grid history https included install installation instance io js libraries lrs lrs-grid-editor map module mymodule options org overview package process pulled rce records repository sdd simply standard test tools transcend tsstools unit view zip"
    },
    {
      "section": "api",
      "id": "transcend.lrs-validation-grid",
      "shortName": "transcend.lrs-validation-grid",
      "type": "overview",
      "moduleName": "transcend.lrs-validation-grid",
      "shortDescription": "LRS Validation Grid Module",
      "keywords": "add analyzer angular api application bower code commands complete conflicts coverage data dependency download easiest error files functionality git grid handling https included install installation interface io js libraries lrs lrs-validation-editor lrs-validation-grid module mymodule options org overview package produced pulled record repository segment services simply status store tds test transcend tsstools unit validation view works zip"
    },
    {
      "section": "api",
      "id": "transcend.lrs-validation-grid.lrsGridEditorConfig",
      "shortName": "transcend.lrs-validation-grid.lrsGridEditorConfig",
      "type": "object",
      "moduleName": "transcend.lrs-validation-grid",
      "shortDescription": "Provides a single configuration object that can be used to change the behaviour, options, urls, etc for any",
      "keywords": "angular angularjs api behaviour change components configuration esriconfig http lrs lrs-validation-grid lrsconfig lrsgrideditorconfig lrsprofile module myapp object options org overridden override single transcend url urls"
    },
    {
      "section": "api",
      "id": "transcend.lrs-validation-grid.lrsValidationGrid",
      "shortName": "transcend.lrs-validation-grid.lrsValidationGrid",
      "type": "directive",
      "moduleName": "transcend.lrs-validation-grid",
      "shortDescription": "A wrapper for transcend.dynamic-grid.dynamicGrid, specifically for visualizing and managing conflicts in",
      "keywords": "$filter $http $log $string api conflicts controls core data directive dynamic-grid dynamicgrid esridata factory function injected instance localstorageservice lrs-validation-grid lrsdatagridconfig lrsvalidationgrid managing map mapmanager mapserver query rce return returns scope segment showcontrols transcend transformlrsdata visualize visualizing wrapper zoomfactory zooms"
    },
    {
      "section": "api",
      "id": "transcend.lrs.$networkLayer",
      "shortName": "transcend.lrs.$networkLayer",
      "type": "service",
      "moduleName": "transcend.lrs",
      "shortDescription": "Provides a set of utility functions related to a plain network layer object (not an instantiated object).",
      "keywords": "$array $networklayer $protofactory api args array based cache common component configuration core creates field fields filter functionality functions hierarchy hierarchyfilter instantiated layer lrs lyr main method network networklayer object objects operations pass perform performed plain property provide purpose reference route routeidfields service set somemethod someothermethod static stored transcend utility var"
    },
    {
      "section": "api",
      "id": "transcend.lrs.directive:lrsProfileDetails",
      "shortName": "lrsProfileDetails",
      "type": "directive",
      "moduleName": "transcend.lrs",
      "shortDescription": "The &#39;lrsProfileDetails&#39; directive provides an editable template for LRS Profile details.",
      "keywords": "api details directive ea editable lrs lrsconfig lrsprofiledetails profile template transcend"
    },
    {
      "section": "api",
      "id": "transcend.lrs.directive:lrsProfileList",
      "shortName": "lrsProfileList",
      "type": "directive",
      "moduleName": "transcend.lrs",
      "shortDescription": "The &#39;lrsProfileList&#39; directive provides an editable template for a Profile list where you can search and select Lrs",
      "keywords": "$resourcelistcontroller api directive ea editable list lrs lrsprofile lrsprofilelist profile profiles search select template transcend"
    },
    {
      "section": "api",
      "id": "transcend.lrs.service:LrsProfile",
      "shortName": "LrsProfile",
      "type": "service",
      "moduleName": "transcend.lrs",
      "shortDescription": "The &#39;LrsProfile&#39; factory provides configuration parameters and direct passage of a URL.",
      "keywords": "$object $q $resource api configuration direct factory lrs lrsconfig lrsprofile parameters passage service transcend url"
    },
    {
      "section": "api",
      "id": "transcend.math",
      "shortName": "transcend.math",
      "type": "overview",
      "moduleName": "transcend.math",
      "shortDescription": "Math Module",
      "keywords": "add analysis angular api application better bower code commands data dependency documentation download easiest files git goals https included install installation io js libraries math mathematical minified module mymodule options org overview package permissions processing project pulled repository simply size source tests tools transcend tsstools unit view zip"
    },
    {
      "section": "api",
      "id": "transcend.math.$math",
      "shortName": "transcend.math.$math",
      "type": "service",
      "moduleName": "transcend.math",
      "shortDescription": "Shortcut static class to be used to access common transcend functionality. This class",
      "keywords": "$math access api class common essentially evaluation example factory findnestedoutliers functionality functions getinlierminmax getmedian getquantiles math method object objects passed service shortcut static transcend wrapper"
    },
    {
      "section": "api",
      "id": "transcend.notify",
      "shortName": "transcend.notify",
      "type": "overview",
      "moduleName": "transcend.notify",
      "shortDescription": "Notify Module",
      "keywords": "add additional analyzer angular api application base bower code commands common creation currently dependency directives download easiest email files git https implement included install installation io js libraries method methods module mymodule notification notify options org overview package port project pulled repository road simply transcend tsstools user view zip"
    },
    {
      "section": "api",
      "id": "transcend.notify-toaster",
      "shortName": "transcend.notify-toaster",
      "type": "overview",
      "moduleName": "transcend.notify-toaster",
      "shortDescription": "Notify Module",
      "keywords": "add angular api application bower code commands common core defined dependency download easiest files git https implementation included install installation io js libraries module mymodule notify notify-toaster options org overview package pulled repository simply toaster top transcend tsstools view zip"
    },
    {
      "section": "api",
      "id": "transcend.notify.service:Email",
      "shortName": "Email",
      "type": "service",
      "moduleName": "transcend.notify",
      "shortDescription": "A $resource that provides the ability to",
      "keywords": "$resource ability account angularjs api authenticates body construct credentials details documentation email emails hello http method methods natively nnresource notify org params parts password post request resource send sendpost sends service set subject transcend tsconfig user"
    },
    {
      "section": "api",
      "id": "transcend.reporting",
      "shortName": "transcend.reporting",
      "type": "overview",
      "moduleName": "transcend.reporting",
      "shortDescription": "Reporting Module",
      "keywords": "ability add angular api application bower code commands controls dependency download easiest engine exposed files functionality git https included install installation interact io js libraries module mymodule options org overview package permissions provide pulled reporting reports repository set simply source transcend tsstools view zip"
    },
    {
      "section": "api",
      "id": "transcend.reporting.directive:reportList",
      "shortName": "reportList",
      "type": "directive",
      "moduleName": "transcend.reporting",
      "shortDescription": "The &#39;reportList&#39; directive provides a user interface that displays a list of available reports for the current",
      "keywords": "$filter $httpbackend angular api app application bar barlegendimage boolean bootstrap calls category config controller copy ctrl current description diagram directive displays double ea features filename format formatted frommeasure function grayscale hidden html http image input interface js list method mock mocks module navigation ng-controller ngmocke2e operated pageheight pagewidth param parameters paramformatter params provided ra repeatingimage report reporting reportingconfig reportlist reports respond routeid scale script selected session signimage snapshot sticklegendimage straight string test test2 testparam tomeasure transcend true type ui user var whenget"
    },
    {
      "section": "api",
      "id": "transcend.reporting.directive:reportNavbar",
      "shortName": "reportNavbar",
      "type": "directive",
      "moduleName": "transcend.reporting",
      "shortDescription": "The &#39;reportNavbar&#39; directive provides a list of available features that operate on one or more selected report items.",
      "keywords": "$window api directive disabled ea features filter interface items list ng operate options report reporting reportnavbar reports search selected text transcend true user"
    },
    {
      "section": "api",
      "id": "transcend.reporting.directive:reportParams",
      "shortName": "reportParams",
      "type": "directive",
      "moduleName": "transcend.reporting",
      "shortDescription": "The &#39;reportParams&#39; directive displays a list of attributes that are defined for a report item in the reports list.",
      "keywords": "api attributes defined desired directive displays ea format formatter generate item list method modify parameters params produce report reporting reportparams reports transcend true user values"
    },
    {
      "section": "api",
      "id": "transcend.reporting.reportingConfig",
      "shortName": "transcend.reporting.reportingConfig",
      "type": "object",
      "moduleName": "transcend.reporting",
      "shortDescription": "Provides a single configuration object that can be used to change the behaviour, options, urls, etc for any",
      "keywords": "__designerendpoint__ __endpoint__ __filename__ __params__ __url__ __viewerendpoint__ angular angularjs api attributes backend base behaviour call capability change common components config configuration configurations configure constant default define designed designerendpoint editor endpoint endpoints engine entry extend file filename http https include module myapp object operation options org output overridden override parameters params passed path program properties report reporting reportingconfig reports represents requested required resource resources securityconfig server service set single specific string transcend url urls vary viewer viewerendpoint written"
    },
    {
      "section": "api",
      "id": "transcend.reporting.service:Report",
      "shortName": "Report",
      "type": "service",
      "moduleName": "transcend.reporting",
      "shortDescription": "Provides a set of &#39;Report&#39; utility functions that in turn invoke functions on the &quot;Reporting Engine&quot; used to create,",
      "keywords": "$notify $resource $window angularjs api application called computer config core create created csv data design designer details documentation download downloads downloadzip edit edited engine entity export exported exporttype file filename files filetype functionality functions href http interact invoke list local method methods natively opens org output params passing path pdf provide purpose query reference report report2 reporting reportingconfig reports resource service set single static transcend turn type url user utility var view viewed viewer window zip"
    },
    {
      "section": "api",
      "id": "transcend.resource",
      "shortName": "transcend.resource",
      "type": "overview",
      "moduleName": "transcend.resource",
      "shortDescription": "Resource Module",
      "keywords": "actions add addition additional angular angularjs api application assist automatically based bower code commands common components control create delete dependency directive documentation download dynamic dynamicform easier easiest files finish form generic git https included install installation io js libraries list meta-data module mymodule number object offers operations options org overview package permissions pulled read repository resource resources services set simply situations source structure tests transcend tsstools unit update utility view working zip"
    },
    {
      "section": "api",
      "id": "transcend.resource.$metaData",
      "shortName": "transcend.resource.$metaData",
      "type": "service",
      "moduleName": "transcend.resource",
      "shortDescription": "The &quot;$metaData&quot; factory provides the ability to create/store information that defines the structure of a given object.",
      "keywords": "$metadata $q $timeout ability age api create defines directive dynamicform expect factory form generate key label length meta-data metadata object passed placeholder property resource rusty service specific structure text tobe toequal tooltip transcend type var"
    },
    {
      "section": "api",
      "id": "transcend.resource.$resourceController",
      "shortName": "transcend.resource.$resourceController",
      "type": "service",
      "moduleName": "transcend.resource",
      "shortDescription": "A controller factory that will provide a new function to be used as a controller in a directive. This controller",
      "keywords": "$invalid $object $resource $resourcecontroller $save $scope __dirtycheckingenabled__ __eventprefix__ __form__ __resource__ acceptable account actions add additional angular angularjs api argument attr automatically basic built call called care changes check conf config configuration configurations controller copy core create created detailed determine determines directive dirty dirtycheckingenabled documentation el element enhance eventprefix events expect extend extends factory fake false fired flag form forms function functionality https instance invalid isdirty link list manipulate method methods methods_extend module mydirective mymodule myresource object onsave onsavebegin onsaveerror org original pass passed performance prefix prevent properties property provide reference reset resets resource resources retrieve retrieved return save saves scope service set setup single skipped specific strict sync takes teh template tobetruthy toequal tohavebeencalled track transcend true usage user valid validity values var view"
    },
    {
      "section": "api",
      "id": "transcend.resource.$resourceList",
      "shortName": "transcend.resource.$resourceList",
      "type": "service",
      "moduleName": "transcend.resource",
      "shortDescription": "The &quot;resourceList&quot; factory provides the ability to extend an AngularJS resource result (if it is an array) in order",
      "keywords": "$add $delete $q $remove $resource $resourcelist $scope $sync $timeout $update __autosync__ __note__ __params__ ability actual adds angularjs api appropriate array automatically basic behaviour built cached call called calling case changed changes clean configure controller copy correct create current default determines directive dirt dirty documentation example existing expect extend factory flag functionality happen holds https include instance instantiated internally item length list manipulate method methods myresource myresourcelist ng-model note object options order org original parameter params pass persist pre-change prechange prior property provide push query re-queries reference reload removed removes reset resets resource resourcelist resources resourceslist result save saves service start sync tells till tobefalsy toequal tohavebeencalled track tracking transcend trigger updated updates valid var view wait"
    },
    {
      "section": "api",
      "id": "transcend.resource.$resourceListController",
      "shortName": "transcend.resource.$resourceListController",
      "type": "service",
      "moduleName": "transcend.resource",
      "shortDescription": "A controller factory that will provide a new function to be used as a controller in a directive that has a list of",
      "keywords": "$delete $invalid $resource $resourcelist $resourcelistcontroller $save $scope __dirtycheckingenabled__ __eventprefix__ __form__ __listproperty__ __note__ __resource__ acceptable account actions actual add additional adds angular angularjs api appended argument attr automatically basic built cached call called calling care changed changes check conf config configuration configurations controller copy correct create created default detailed determine determines directive dirty dirtycheckingenabled documentation el element enhance eventprefix events example existing expect extend extends factory fake false fired flag form forms function functionality https instance internally invalid isdirty item length link list listproperty manipulate method methods methods_extend module mylistdirective mymodule myresource myresourcelist myresourceslist ng-model note object onsave onsavebegin onsaveerror org original pass passed performance pre-change prechange prefix prevent prior properties property provide push query re-queries reference reload remove removed removes reset resets resource resources resourceslist result retrieved return save saves scope service set setup skipped specific start strict sync takes teh tells template tobefalsy tobetruthy toequal tohavebeencalled track tracking transcend trigger true update updated usage user valid validity values var view"
    },
    {
      "section": "api",
      "id": "transcend.resource.directive:dynamicForm",
      "shortName": "dynamicForm",
      "type": "directive",
      "moduleName": "transcend.resource",
      "shortDescription": "The &#39;dynamicForm&#39; directive provides a way to create forms automatically based on an object&#39;s properties and value.",
      "keywords": "$scope access additional address angular angularjs api app automatically based basic build city completing configuration controller create ctrl declarer default details directive dynamicform ea elm email evaluate expression failing firstname form forms function gain green happy html https js lastname meta-data metadata model module ng-controller ngmodel object onsave onsavebegin onsaveerror optional org passed process properties provide resource rgreen rusty save script starting street town transcend types validations values zipcode"
    },
    {
      "section": "api",
      "id": "transcend.resource.directive:formComponent",
      "shortName": "formComponent",
      "type": "directive",
      "moduleName": "transcend.resource",
      "shortDescription": "The &#39;dynamicForm&#39; directive provides a way to create forms automatically based on an object&#39;s properties and value.",
      "keywords": "access additional angularjs api automatically based basic build completing configuration controller create declarer default details directive dynamicform ea evaluate expression failing form forms gain https meta-data metadata ngmodel object onsave onsavebegin onsaveerror optional org passed process properties provide resource save starting transcend types validations values"
    },
    {
      "section": "api",
      "id": "transcend.resource.directive:preChange",
      "shortName": "preChange",
      "type": "directive",
      "moduleName": "transcend.resource",
      "shortDescription": "The &#39;preChange&#39; directive provides a way to fire a function when the ng-model value is changed but it will fire",
      "keywords": "actual angularjs api change changed directive evaluate expression fire fires function https input model ng-change ng-model ngmodel org prechange resource transcend updated"
    },
    {
      "section": "api",
      "id": "transcend.security",
      "shortName": "transcend.security",
      "type": "overview",
      "moduleName": "transcend.security",
      "shortDescription": "Security Module",
      "keywords": "$provider accounts add additional angular api application aspx authentication back-end backend based bearer bower box change code commands component components configuration configure configured content control default deferring dependency designed development download easiest files front-end git goals hiding http https included individual install installation io js libraries management method minified module msdn mvc mvc5 myapp mymodule net object options org override overriding overview owin package passed password permissions project properties pulled registration repository resource resources role roles seamlessly security securityconfig set setting settings setup simply source standard support token transcend tsstools url usage user view webapi webapi2 work zip"
    },
    {
      "section": "api",
      "id": "transcend.security-manager",
      "shortName": "transcend.security-manager",
      "type": "overview",
      "moduleName": "transcend.security-manager",
      "shortDescription": "Security Manager Module",
      "keywords": "$provider access accounts add additional angular api application authentication back-end backend bearer bower box builds change code commands component components configuration configure configured control controls data default deferring dependency development download easiest enable files front-end git goals http https included individual install installation io js libraries management manager managing method minified module mvc5 myapp mymodule object options org override overriding overview package passed password permissions project properties pulled registration repository resource resources role roles security security-manager securitymanagerconfig set setting settings setup simply source standard support token top transcend tsstools url usage users view webapi webapi2 work zip"
    },
    {
      "section": "api",
      "id": "transcend.security-manager.directive:dataLayerPrivilegeManager",
      "shortName": "dataLayerPrivilegeManager",
      "type": "directive",
      "moduleName": "transcend.security-manager",
      "shortDescription": "The &#39;roleFinder&#39; directive provides an HTML element that can be used to display and select a role returned from the the Role service. The directive",
      "keywords": "api box current directive display dropdown ea element html returned role rolefinder roles security-manager select service transcend utilize"
    },
    {
      "section": "api",
      "id": "transcend.security-manager.directive:roleEditor",
      "shortName": "roleEditor",
      "type": "directive",
      "moduleName": "transcend.security-manager",
      "shortDescription": "The &#39;roleFinder&#39; directive provides an HTML element that can be used to display and select a role returned from the the Role service. The directive",
      "keywords": "api box current directive display dropdown ea element html returned role rolefinder roles security-manager select service transcend utilize"
    },
    {
      "section": "api",
      "id": "transcend.security-manager.directive:roleFinder",
      "shortName": "roleFinder",
      "type": "directive",
      "moduleName": "transcend.security-manager",
      "shortDescription": "The &#39;roleFinder&#39; directive provides an HTML element that can be used to display and select a role returned from the the Role service. The directive",
      "keywords": "api box current directive display dropdown ea element html returned role rolefinder roles security-manager select service transcend utilize"
    },
    {
      "section": "api",
      "id": "transcend.security-manager.directive:rolePrivileges",
      "shortName": "rolePrivileges",
      "type": "directive",
      "moduleName": "transcend.security-manager",
      "shortDescription": "The &#39;rolePrivileges&#39; directive provides an HTML element that provides an interface to view and edit the privileges assigned to a role. This directive utilizes the",
      "keywords": "api assigned current directive display dropdown ea edit element html interface list privilege privileges requires role rolefinder roleprivilege roleprivileges rolepriviliges roles security-manager services transcend utilizes view"
    },
    {
      "section": "api",
      "id": "transcend.security-manager.directive:siteSecurityManager",
      "shortName": "siteSecurityManager",
      "type": "directive",
      "moduleName": "transcend.security-manager",
      "shortDescription": "The &#39;siteSecurityManager&#39; directive provides the ability to see and edit the site&#39;s security/authentication mechanism.",
      "keywords": "$array $notify ability ae api core directive edit mechanism security security-manager securityprofile site sitesecuritymanager transcend"
    },
    {
      "section": "api",
      "id": "transcend.security-manager.directive:userRolesManager",
      "shortName": "userRolesManager",
      "type": "directive",
      "moduleName": "transcend.security-manager",
      "shortDescription": "The &#39;userRolesManager&#39; directive provides an HTML element that can be used to display and edit the roles assigned to",
      "keywords": "api assigned directive display ea edit element html includes lookup retrieve role roles security-manager services transcend update user userfinder userrole userrolesmanager users utilizes"
    },
    {
      "section": "api",
      "id": "transcend.security-manager.securityManagerConfig",
      "shortName": "transcend.security-manager.securityManagerConfig",
      "type": "object",
      "moduleName": "transcend.security-manager",
      "shortDescription": "Provides a single configuration object that can be used to change the behaviour, options, urls, etc for any",
      "keywords": "$resource __ __note __params__ __url__ account angular angularjs api backend base behaviour call called capability change common components config configuration configurations configure default define details documentation extend http initially instance manager method methods module myapp natively object options org overridden override parameters passed prefixing privilege properties represents resource resources role roleprivilege rolprivilege security security-manager securitymanagerconfig server service set single specific standard string transcend ultimately url urls vary"
    },
    {
      "section": "api",
      "id": "transcend.security-manager.service:DataLayerPrivilege",
      "shortName": "DataLayerPrivilege",
      "type": "service",
      "moduleName": "transcend.security-manager",
      "shortDescription": "A $resource to interact with a &quot;DataLayerPrivilege&quot; entity.",
      "keywords": "$resource abc angularjs api create data datalayerprivilege datalayerprivileges delete editable entity functionality http interact layer layers list models org privilege privileges provide purpose query read security-manager securitymanagerconfig service single transcend update var visible visiblelayers"
    },
    {
      "section": "api",
      "id": "transcend.security-manager.service:Privilege",
      "shortName": "Privilege",
      "type": "service",
      "moduleName": "transcend.security-manager",
      "shortDescription": "A $resource to interact with a &quot;Role&quot; entity.",
      "keywords": "$delete $resource $save angularjs api create delete description entity functionality http interact list models org privilege privileges provide purpose query read role security-manager securitymanagerconfig service single transcend update var"
    },
    {
      "section": "api",
      "id": "transcend.security-manager.service:Role",
      "shortName": "Role",
      "type": "service",
      "moduleName": "transcend.security-manager",
      "shortDescription": "A $resource to interact with a &quot;Role&quot; entity.",
      "keywords": "$resource abc angularjs api create delete description entity functionality http interact list models org provide purpose query read role roles security-manager securitymanagerconfig service single transcend update var"
    },
    {
      "section": "api",
      "id": "transcend.security-manager.service:RolePrivilege",
      "shortName": "RolePrivilege",
      "type": "service",
      "moduleName": "transcend.security-manager",
      "shortDescription": "A $resource to interact with a &quot;RolePrivilege&quot; entity.",
      "keywords": "$resource angularjs api create delete entity functionality http interact list models org privilege provide purpose query read relationship role roleprivilege roleprivs roles security-manager securitymanagerconfig service single transcend update var"
    },
    {
      "section": "api",
      "id": "transcend.security-manager.service:UserRole",
      "shortName": "UserRole",
      "type": "service",
      "moduleName": "transcend.security-manager",
      "shortDescription": "A $resource to interact with a &quot;UserRole&quot; entity.",
      "keywords": "$resource angularjs api create delete entity functionality http interact list models org provide purpose query read relationship role roles security-manager securitymanagerconfig service single transcend update user userid userrole userroles var"
    },
    {
      "section": "api",
      "id": "transcend.security-manager:SecurityProfile",
      "shortName": "SecurityProfile",
      "type": "service",
      "moduleName": "transcend",
      "shortDescription": "The &#39;SecurityProfile&#39; factory returns a new Vector Editor for SVG purposees.",
      "keywords": "$q $resource api editor factory purposees returns security-manager securitymanagerconfig securityprofile service svg transcend vector"
    },
    {
      "section": "api",
      "id": "transcend.security.$authenticate",
      "shortName": "transcend.security.$authenticate",
      "type": "service",
      "moduleName": "transcend.security",
      "shortDescription": "Provides an interface for authenticating (singing in) a user as well as signing out a user. The $authenticate service",
      "keywords": "$authenticate $http $loading $log $notify $rolemanager $rootscope account action actual api appropriate authenticate authenticated authenticating authentication based browser configured core credentials default details email empty exists expect expired failcallback firstname flag green handle handling holds http includes interface isauthenticated lastname loadlocaltoken loads local manager method object opted original passed password place property raw remember removing request requires reset rest rgreen role roles rusty security service setting sign signing signout signs sing storage string supports tobefalsy toequal token transcend user username"
    },
    {
      "section": "api",
      "id": "transcend.security.$roleManager",
      "shortName": "transcend.security.$roleManager",
      "type": "service",
      "moduleName": "transcend.security",
      "shortDescription": "Provides functionality to store and manage user roles. The main purpose of this service is to provide a simple",
      "keywords": "$array $log $rolemanager $rootscope admin allinclusive allows api array authenticate authentication based called calls check checking clear clearroles clears comma configuration configure configures core credentials default defaults determine determines directive enable enabled event example expect false finds fire flag function functionality guest hasrole hasroles ids inclusive interactive interface length main manage manager method multiple note object operator original pass passed password permissions pipe property provide purpose raw reset resets rest return role roleids roles roles-changed security securityconfig separated separating service set sets setting simple store stored string tobefalsy tobetruthy toequal transcend true update updates user username valid validate values"
    },
    {
      "section": "api",
      "id": "transcend.security.directive:inlineUserSignIn",
      "shortName": "inlineUserSignIn",
      "type": "directive",
      "moduleName": "transcend.security",
      "shortDescription": "The &#39;inlineUserSignIn&#39; directive provides a compacted and simplified interface to let users enter their credentials",
      "keywords": "$authenticate $httpbackend $log $rolemanager $scope abstracts access_token allows angular api app aug authenticated authentication b6aad2d8-2fdd-4b07-91f9-ada394f1ee95 bearer called calls class compacted congratulations controller credentials ctrl details directive ea email enabled enter error error_description expires expires_in fail false firstname function gmt grant_type green html http include incorrect inlineusersignin interface invalid_grant issued js lastname message mock mocks module mon nav-inline-user-sign-in navbar navbar-default navigation ng-controller ng-style ngmocke2e nov on-fail on-success onfail onsuccess password pull-right red rememberme respond rgreen role roles rusty script security securityconfig sign signed simplified simply siteadmin style sun support test-token token_type transcend user username users valid whenpost"
    },
    {
      "section": "api",
      "id": "transcend.security.directive:roles",
      "shortName": "roles",
      "type": "directive",
      "moduleName": "transcend.security",
      "shortDescription": "The &#39;roles&#39; directive provides a simple way to define an HTML element as having a particular role id. The directive",
      "keywords": "$parse $rolemanager $scope access angular api app application automatically background-color c4c4ff c5ecc5 class client code comma-delimited components congratulations content controller ctrl define depending directive div dom element evaluate example expression fail ffc4c4 ffdc9d function happen hide html ids implementation interact javascript js list logic manipulate module ng-change ng-controller ng-if ng-model note operator order pass passed passing pipe-delimited placeholder prevents privileges provide required rest role roleids rolemanager roles roles-fail roles-pass rolesfail rolespass row-fluid script secure security securityconfig services setroles side signed simple span3 style text three transcend true type user view visible"
    },
    {
      "section": "api",
      "id": "transcend.security.directive:userFinder",
      "shortName": "userFinder",
      "type": "directive",
      "moduleName": "transcend.security",
      "shortDescription": "The &#39;userFinder&#39; directive provides an HTML element that can be used to display and select a user returned from the the Users service. The directive",
      "keywords": "$rolemanager $scope angular api app background-color bootstrap c4c4ff c5ecc5 class controller ctrl directive display div ea element entry example ffc4c4 ffdc9d function html ids js key module ng-change ng-controller ng-model order placeholder query required returned role roleids rolemanager roles row-fluid script security securityconfig select service setroles span3 style text three transcend type typeahead user userfinder users utilize view visible"
    },
    {
      "section": "api",
      "id": "transcend.security.directive:userRegistration",
      "shortName": "userRegistration",
      "type": "directive",
      "moduleName": "transcend.security",
      "shortDescription": "The &#39;userRegistration&#39; directive provides a template for secure User registration",
      "keywords": "$authenticate $notify $string api core directive ea registration secure security service template transcend user userregistration"
    },
    {
      "section": "api",
      "id": "transcend.security.securityConfig",
      "shortName": "transcend.security.securityConfig",
      "type": "object",
      "moduleName": "transcend.security",
      "shortDescription": "Provides a single configuration object that can be used to change the behaviour, options, urls, etc for any",
      "keywords": "$rolemanager __ __bypass__ __credentials__ __default__ __enabled__ __note __params__ __roleids__ __roles__ __url__ account add angular angularjs api array attribute authentication authorization auto-fill automatically backend base based behaviour boolean box bypass call called capability change checking common components config configuration configurations configure content credentials default define determines dictates directive enabled essentially example extend false flag functionality guest header headers hiding holds http ids ignored initially inline inlineusersignin input instance kplug77be-2t3qjpkf7ahk-kopt5yuqgr0x6pcgui-hw9xmlxt19wvm4l list live match matches meaning method methods module myapp object objects options org overridden override parameters passed password pattern populated post pre-defined prefix prefixing process project properties property represents request resource resources role roleids rolemanager roles route security securityconfig server service set sign single skipped specific standard string token tokenrequiredurlpattern transcend true ultimately url urls user username vary webapi"
    },
    {
      "section": "api",
      "id": "transcend.security.service:Account",
      "shortName": "Account",
      "type": "service",
      "moduleName": "transcend.security",
      "shortDescription": "A $resource to interact with an &quot;Account&quot; entity.",
      "keywords": "$resource $signout $string $userinfo abc123 account angularjs anonymous api aqaancabcxyz authenticate authenticated authenticates authentication backend call changepassword changes client confirmpassword contact core create credentials def456 details documentation email entity example expect firstname forgotpassword functionality http include info interact invalidate lastname manage method methods models myemail natively newpassword object oldpassword org passed password post property provide purpose raw redirectto register registers request reset resetpassword resets resettoken resource response rest retrieve rgreen roles security securityconfig send server service set signed-in signout signs single string team toequal token transcend user userinfo username users valid var"
    },
    {
      "section": "api",
      "id": "transcend.security.service:User",
      "shortName": "User",
      "type": "service",
      "moduleName": "transcend.security",
      "shortDescription": "The &#39;User&#39; factory returns configuration parameters for Users",
      "keywords": "$resource api configuration factory parameters returns security securityconfig service transcend user users"
    },
    {
      "section": "api",
      "id": "transcend.themes",
      "shortName": "transcend.themes",
      "type": "overview",
      "moduleName": "transcend.themes",
      "shortDescription": "Themes Module",
      "keywords": "$provider accounts add additional angular api application authentication back-end backend bearer bower box change code commands component components configuration configure configured control default deferring dependency development download easiest files front-end git goals http https included individual install installation io js libraries management method minified module mvc5 myapp mymodule object options org override overriding overview package passed password permissions project properties pulled registration repository resource resources role set setting settings setup simply source standard support themes themesconfig token transcend tsstools url usage view webapi webapi2 work zip"
    },
    {
      "section": "api",
      "id": "transcend.themes.directive:themeMenu",
      "shortName": "themeMenu",
      "type": "directive",
      "moduleName": "transcend.themes",
      "shortDescription": "The &#39;themeMenu&#39; directive provides a simple dropdown menu component that will allow selecting (binding) to a theme.",
      "keywords": "$rootscope allow api appconfig component core directive dropdown ea menu navbar note selected selecting set simple theme theme-menu thememenu themes themesconfig transcend typical usage"
    },
    {
      "section": "api",
      "id": "transcend.themes.themesConfig",
      "shortName": "transcend.themes.themesConfig",
      "type": "object",
      "moduleName": "transcend.themes",
      "shortDescription": "Provides a single configuration object that can be used to change the behaviour, options, urls, etc for any",
      "keywords": "__ __note angular angularjs api associated avialable behaviour cartographic cerulean change components configuration contrast cosmo css cyborg darkly default file flatly http journal khaki limit list lumen match midnight mint module myapp object ocean options org overridden override paper patriot property purpley readable royal sandstone simplex single slate spacelab sunrise superhero theme themes themesconfig transcend united urls yeti"
    },
    {
      "section": "dev",
      "id": "index",
      "shortName": "Developer Overview",
      "type": "overview",
      "moduleName": "Developer Overview",
      "shortDescription": "",
      "keywords": "adhere adheres alt api application applications base bitbucket built checkout class code code-libraries coding data design dev developer development docs environment familiarize fork git guide help ivborw0kggoaaaansuheugaaaoeaaadhcamaaaajbsjiaaaae1bmvex latest learn library local modern modules org overview philosophy point productive provide pull-right reference repositories repository required resources review setup software src standards started starting steps style technologies tools transcend tutorials utilize view web web-app-template web-based work workflows working"
    },
    {
      "section": "dev",
      "id": "00_standards",
      "shortName": "Coding Standards",
      "type": "overview",
      "moduleName": "Coding Standards",
      "shortDescription": "",
      "keywords": "__consistent__ __re-usable__ __readable__ aim alt associated base better built changes class code coding concise data design dev development documentation easily ends enhancements git google guy https ivborw0kggoaaaansuheugaaanaaaadzcamaaadaqmjeaaaawfbmvex layout live maintained maintaining net overview philosophy psychopath pull pull-right recommended repository request requested rules simple src standards transcend violent web write"
    },
    {
      "section": "dev",
      "id": "01_infrastructure",
      "shortName": "IT Infrastructure",
      "type": "overview",
      "moduleName": "IT Infrastructure",
      "shortDescription": "",
      "keywords": "__development __production __staging _blank accesed accesible access accessed alt app application applications approval arcgis aspx authentication build cdn changing class code common configuring connection content convention copy corporate credentials data default delivery depending deploy deployed deployment desk desktop dev development distributables documentation domain domains drive entering environment example explorer external file files fo ftp global greg happen help host hostgator house href http images include infrastructre infrastructure infrastruture install installations integrated ip ivborw0kggoaaaansuheugaaaoeaaadhcamaaaajbsjiaaaakfbmvex location login majority management manual map names naming network number open options overview packages particularaly patter permissions policy portal process production project public publicly publishing pull-right purpose pushing rdp redmine referencing remote remotely repository resources rules rusty security server servers servers__ shared site software sparingly sql src staging static strings sub-domain support target ticket tools transcend true tss tsstools unique usages vpn web web-deploy windows"
    },
    {
      "section": "dev",
      "id": "02_software",
      "shortName": "Software and Tools",
      "type": "overview",
      "moduleName": "Software and Tools",
      "shortDescription": "",
      "keywords": "account adds advancedinstaller alt analyze applications aspx authoring based browser build built built-in business bwg7x-j98b3-w34rt-33b3r-jvyw9 capture check checkout choose chrome class cleanly client cloud code collaboration communication compact company connect control create data db debugging depending desired desktop dev developer developers development developmment diagrams directory documentation download drive easily edit employee enterpise environment esri expectations extension fairly files flexible formatted formatting front-end generate google hardware html http ide infrastructure install installation installers internal iso ivborw0kggoaaaansuheugaaaoeaaadhcamaaaajbsjiaaaahfbmvex jdvqw-2nprt-hj8bh-ffmdx-2yq73 jetbrains json key launch launchy licensing list locations long madcapsoftware manage management manager meet microsoft microsoftonline msdn net nf3mt-tfr9g-q2tvf-37mj7-x7373 note nuget number office open-source oracle org overview package passwords plugins primary product productive products project projects pull-right quality recommended redmine refer repository request requests require required requirements resources rest return screenshots server-side set share shared skype software spatial specific src store studio supplement support system tasks techsmith terminal toolbar tools toolsets transcend typically ultimate utility vast version vides view viewable visio visual visualstudio web web-based webstorm workflows"
    },
    {
      "section": "dev",
      "id": "03_environment",
      "shortName": "Development Environment",
      "type": "overview",
      "moduleName": "Development Environment",
      "shortDescription": "",
      "keywords": "__ __application __check __except__ __internet __projects __root__ __virtual __web __world accepted access account add all__ alt application applications basic bitbucket bower browsing built check cl class clone command commands common commonly communicate comparability compatibility compression configuration configure console contact content correctly create data detailed details dev develop development diagnostics differs directory directory__ document easier enable environment features filtering folder__ gain git globally great grunt grunt-cli gui guide health help host http ide iis install installed installing instructions integrated jetbrains json json__ jsp key license lifecycle list logging ly management manager metabase mime needed net nodejs number open option optionally order org overpage overview package performance png points process programs project projects prompt publishing pull-right repositories repository request required running scripts securely security send services services__ setting setup sourcetree sourcetreeapp specific src ssh static task tasks team technologies terminal test tool tools tools__ transcend tsstools turn types username virtual visit web webdav webstorm wide windows working"
    },
    {
      "section": "dev",
      "id": "04_resources",
      "shortName": "Development Resources",
      "type": "overview",
      "moduleName": "Development Resources",
      "shortDescription": "",
      "keywords": "$scope account accounts advanced allow alt alter angular angularjs announcement api application applications approach apps art asp aspx asynchronously attribute authentication authorization basics behaviours best binding bootstrap bottom bower browser browsers cache change class clearly click client-side code codeplex codeproject common commonly communicate communication components config configuration containers content control controller controllers conventions copy creation crockford css currently cycle data dependency design desktop dev devart developing development directive displayed document dot dropcreatedatabasealways dropcreatedatabaseifmodelchanges dynamic easiest ef6 egghead element eliminate entity error event express expression extend extending features filtering filters forms framework function game generally github google grunt guide handling helpful homepage html ideal identity implementations initializers injection interact interesting io isolate javascript jetbrains language libraries life link links list log making manager method microsoft migration migrations mobile modern modules msdn mvc5 net nodejs oauth2 official onwards open operations oracle order org overview owin package params partner patterns php practices programming provider providers pull-right read reference refresh release resources restrictions review rollback route schools scope scripts security sense server server-side services short sign single spa src start started structural style succinctly suggested support syntax tab talking technologies technology template tips tokens top topic transformation tricks tutorial tutorials tuts tutsplus ui undo url user w3 w3schools web webapi webstorm write youtube zippy"
    },
    {
      "section": "gitflows",
      "id": "index",
      "shortName": "Workflows Overview",
      "type": "overview",
      "moduleName": "Workflows Overview",
      "shortDescription": "Transcend Git Workflows",
      "keywords": "__basic __bitbucket __kdiff__ __sourcetree__ __ssh __tss accepts access access__ account account__ add admin administrator applying automatically basic bitbucket blog branch branching changes checkout code commit commits common complete conflicts contact conventions create creates deletes described develop developer diff document download editor email external familiar feature files follow generate git gitflows github google gui heading high-level host html instructions interactive internal io kdiff key key__ management member merge merges message model net notified operations order org overview post prerequisites program provide pull push pushes q1zm52164s8v read repo repositories repository request resolve resources runs set setting sourceforge sourcetree sourcetreeapp ssh start successful suggested team todo tool transcend tsstools tutorial understanding understanding__ username watch workflow workflows working"
    },
    {
      "section": "gitflows",
      "id": "addingcontent",
      "shortName": "Adding New Content",
      "type": "overview",
      "moduleName": "Adding New Content",
      "shortDescription": "Adding New Content",
      "keywords": "add adding appear button changes click comment commit commiting content copy dialog enter file gitflows heading http local move open overview png project prompted repository save sourcetree src staged tsstools window work working y19rmmmfsp5w"
    },
    {
      "section": "gitflows",
      "id": "addssh",
      "shortName": "Adding an SSH Key",
      "type": "overview",
      "moduleName": "Adding an SSH Key",
      "shortDescription": "How to add an SSH key",
      "keywords": "accept account add adding address alter application bash box button click clipboard command confirm contents copied copy default drop editor email enter file git gitflows hit http input key label laptop left list location message navigate open options overview passphrase paste picture png profile pub putty rsa save select set sourcetree src ssh ssh-keygen text todo transcend tss tsstools user"
    },
    {
      "section": "gitflows",
      "id": "branchcheckout",
      "shortName": "Checkout a Branch",
      "type": "overview",
      "moduleName": "Checkout a Branch",
      "shortDescription": "Checkout a Branch | Video Walkthrough",
      "keywords": "branch changing checkout click code copy feature gitflows include local overview pull refers request review swapping walkthrough work"
    },
    {
      "section": "gitflows",
      "id": "clonerepo",
      "shortName": "Clone a Repository",
      "type": "overview",
      "moduleName": "Clone a Repository",
      "shortDescription": "Cloning a Repository | Video Walkthrough",
      "keywords": "browse button click clone cloning code copy created destination explorer gitflows globe gvl9dmi0hhdw heading hit hosted http icon local locally machine making open overview path png projects pulling repositories repository select set sourcetree src tsstools view walkthrough work"
    },
    {
      "section": "gitflows",
      "id": "commitnewfeature",
      "shortName": "Committing a New Feature",
      "type": "overview",
      "moduleName": "Committing a New Feature",
      "shortDescription": "Committing a New Feature",
      "keywords": "branch button changes checked click commit committing complete completed conflict copy ensure feature files gitflows http merge overview png problems pull push pushing remote resolution select selected skipped src staged step steps tsstools window working"
    },
    {
      "section": "gitflows",
      "id": "conflictresolution",
      "shortName": "Conflict Resolution",
      "type": "overview",
      "moduleName": "Conflict Resolution",
      "shortDescription": "Conflict Resolution",
      "keywords": "appear bottom box choose click complete conflict conflicts continue current diff errors event external file follow gitflows hand http instructions kdiff3 left lines list locate merge merging opens option overview png pulling red resolution return save screen select sourcetree src status step task text top tsstools version window"
    },
    {
      "section": "gitflows",
      "id": "createfeature",
      "shortName": "Creating a New Feature",
      "type": "overview",
      "moduleName": "Creating a New Feature",
      "shortDescription": "Creating a New Feature",
      "keywords": "accepted add application appropriate approved associated assuming automatically base bitbucket branch branches browser button changes checkout click code comments commit conflicts conform continue convention create creating current declined delete describe described describing develop developing display email feature flow folder form frequently general gitflows heading hit http idea include inclusion input issue latest lowercase merge minimize naming number occurred open option origin overview perform png pound pr prefixed pull pulled push pushes receive remote replace repository request resolve review reviewer select selected send separated sign sourcetree spaces specific src stating step steps submit task test toolbar tsstools underscore underscores window work working works y19rmmmfsp5w"
    },
    {
      "section": "gitflows",
      "id": "newrelease",
      "shortName": "Creating a New Release",
      "type": "overview",
      "moduleName": "Creating a New Release",
      "shortDescription": "Create a New Release",
      "keywords": "create creating gitflows overview release todo"
    },
    {
      "section": "gitflows",
      "id": "watchrepo",
      "shortName": "Watch a Repository",
      "type": "overview",
      "moduleName": "Watch a Repository",
      "shortDescription": "Watch a Repository | Video Walkthrough",
      "keywords": "bitbucket changes gitflows icon notified order overview repository transcend tsstools walkthrough watch watching work"
    }
  ],
  "apis": {
    "api": true,
    "dev": false,
    "gitflows": false
  },
  "html5Mode": false,
  "editExample": true,
  "startPage": "/dev",
  "scripts": [
    "angular.min.js",
    "angular-route.min.js",
    "angular-resource.min.js",
    "angular-animate.min.js",
    "angular-sanitize.min.js",
    "angular-mocks.js",
    "ui-bootstrap-tpls.min.js",
    "angulartics.min.js",
    "angulartics-ga-cordova.min.js",
    "loading-bar.min.js",
    "toaster.min.js",
    "angular-bootstrap-confirm.min.js",
    "init.js",
    "AnyChart.js",
    "AnyChartHTML5.js"
  ]
};